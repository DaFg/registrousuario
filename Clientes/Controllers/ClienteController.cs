﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clientes.ContextDbs;
using Clientes.Models;
using System.Data.Entity;
using PagedList;
using System.Xml.Xsl.Runtime;


namespace Clientes.Controllers
{
	public class ClienteController : Controller
	{

		private RegistrosContext db;

		public ClienteController()
		{
			this.db = new RegistrosContext();
		}

		//		public ActionResult PruebaInsert()
		//		{
		//			Cliente c = new Cliente()
		//			{
		//				Nombre = "Probando",
		//				Apellido = "Pruebita",
		//				Correo = "probando@pruebita.com",
		//				Documento = 44926480
		//			};
		//			db.clientes.Add(c);
		//			db.SaveChanges();
		//
		//			return RedirectToAction("Index", "Home");
		//		}

		public ActionResult Index(string SearchString)
		{
			List <Cliente> clientito;
			try
			{
				clientito = this.db.clientes.ToList();

				if (!String.IsNullOrEmpty(SearchString))
				{
					clientito = clientito.Where(c => c.Nombre.ToUpper().Contains(SearchString.ToUpper())
						|| c.Apellido.ToUpper().Contains(SearchString.ToUpper())).ToList();
				}
			}
			catch (Exception ex)
			{
				string message = ex.ToString();
				return RedirectToAction("Index");
			}


			return View(clientito);
		}


		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return RedirectToAction("Index");
			}

			Cliente clientote;
			clientote = this.db.clientes.Find(id);

			if (clientote == null)
			{
				return HttpNotFound();
			}
			return View(clientote);
		}


		public ActionResult Create()
		{
			return View();
		}



		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Nombre, Apellido, Correo, Documento")]Cliente c)
		{
			if (ModelState.IsValid)
			{	
				db.clientes.Add(c);
				db.SaveChanges();
				//Cliente clientito;
				//return RedirectToAction("Index");
				return RedirectToAction("Index");

			}
			return View();
		}

		public ActionResult Edit(int id)
		{
			Cliente clientote = new Cliente();
			clientote = this.db.clientes.Find(id);
			if (clientote == null)
			{
				return HttpNotFound();
			}
			return View(clientote);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(Cliente clientote)
		{
			try
			{
				if (ModelState.IsValid)
				{
					db.Entry(clientote).State = EntityState.Modified;
					db.SaveChanges();
					return RedirectToAction("Index");
				}
			}
			catch (Exception ex)
			{
				string message = ex.ToString();
				return RedirectToAction("Index");
			}
			return View(clientote);
		}

		public ActionResult EditTelefono(int id)
		{
			var query_where1 = from a in db.telefonos
			                   where a.ClienteId.Equals(id)
			                   select a.ID;
			var idTel = query_where1.First();

			Telefono telefonito = new Telefono();
			telefonito = this.db.telefonos.Find(idTel);
			//var telefonitoID = telefonito.ID;
			//Cliente client = new Cliente();
			//client = this.db.clientes.Find(id);

			if (telefonito == null) //|| client == null)
			{
				return HttpNotFound();
			}
			return View(telefonito);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditTelefono(Telefono telefonito)
		{
			var identTel = telefonito.ID;
			var query_where1 = from a in db.telefonos
			                   where a.ClienteId.Equals(identTel)
			                   select a.ID;
			var idTel = query_where1.First();

			telefonito = this.db.telefonos.Find(idTel);

			try
			{
				if (ModelState.IsValid)
				{
					
					db.Entry(telefonito).State = EntityState.Modified;
					db.SaveChanges();
					return RedirectToAction("Index");
				}
			}
			catch (Exception ex)
			{
				string message = ex.ToString();
				return RedirectToAction("Index");
			}
			return View(telefonito);
		}

		public ActionResult Delete(int id)
		{
			return View();
		}

		[HttpPost]
		public ActionResult Delete(int id, FormCollection collection)
		{
			try
			{
				return RedirectToAction("Index");
			}
			catch
			{
				return View();
			}
		}

		// GET: /Cliente/AgregarTelefono/5

		public ActionResult AgregarTelefono(int id)//Para esta acción vamos a crear una vista de detalle
		{
			Cliente clientote;
			clientote = this.db.clientes.Find(id);
			if (clientote == null)
			{
				return HttpNotFound();
			}
			return View();
		}


		// POST: /Cliente/AgregarTelefono
		[HttpPost]
		public ActionResult AgregarTelefono(Telefono tel, Cliente clientote)
		{
		
			//int clientoteID = clientote.ID;
		
			try
			{
				//if (ModelState.IsValid)
				//{
				tel.Cliente = this.db.clientes.Find(clientote.ID);//busco el cliente y le asigno un telefono
				db.telefonos.Add(tel);
				db.SaveChanges();
				//}
				//return RedirectToAction("Index");
			}
			catch (Exception ex)
			{
				string message = ex.ToString();
			}
			return RedirectToAction("Index");
		
		}

		//		public ActionResult AgregarTelCelular(int id)
		//		{
		//			Cliente clientote;
		//			clientote = this.db.clientes.Find(id);
		//			if (clientote == null)
		//			{
		//				return HttpNotFound();
		//			}
		//			return View();
		//		}
		//
		//		[HttpPost]
		//		public ActionResult AgregarTelCelular(TelCelular tc, Cliente clientote)
		//		{
		//			try
		//			{
		//				//if (ModelState.IsValid)
		//				//{
		//				tc.Cliente = this.db.clientes.Find(clientote.ID);
		//				db.telCel.Add(tc);
		//				db.SaveChanges();
		//				//}
		//				//return RedirectToAction("Index");
		//			}
		//			catch (Exception ex)
		//			{
		//				string message = ex.ToString();
		//			}
		//			return RedirectToAction("Index");
		//		}

		protected override void Dispose(bool disposing)
		{
			db.Dispose();
			base.Dispose(disposing);
		}
	}
}