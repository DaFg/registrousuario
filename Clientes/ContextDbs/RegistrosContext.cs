﻿using System;
using System.Data.Entity;
using System.Data.Common;
using Clientes.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Clientes.ContextDbs
{
	public class RegistrosContext : DbContext
	{
		public RegistrosContext()
			: base("SoftBrainContext")
		{
			
		}

		//Este metodo evita la pluralizacion del nombre de las tablas.
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
		}

		public RegistrosContext(DbConnection existingConnection, bool contextOwnsConnection)
			: base(existingConnection, contextOwnsConnection)
		{
		}

		public DbSet<Cliente> clientes { get; set; }

		public DbSet<Telefono> telefonos { get; set; }

		public DbSet<TelCelular> telCel { get; set; }

		public DbSet<TelHogar> telHogar{ get; set; }

		public DbSet<TelTrabajo> telTrabajo{ get; set; }

	}
}

