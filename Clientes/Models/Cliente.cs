﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Collections.Generic;


namespace Clientes.Models
{
	[Table("Cliente", Schema = "probando")]
	public class Cliente
	{
		[DisplayName("Numero")]
		[Column("Id")]
		public int ID { get; set; }

		[DisplayName("Nombre ")]
		[Required(ErrorMessage = "Nombre requerido")]
		public String Nombre { get; set; }

		[DisplayName("Apellido ")]
		[Required(ErrorMessage = "Apellido requerido")]
		public String Apellido { get; set; }

		[DisplayName("Correro ")]
		public String Correo { get; set; }

		[DisplayName("Documento ")]
		[Required(ErrorMessage = "Documento requerido")]
		public int Documento { get; set; }

		public virtual ICollection<Telefono> Telefonos { get; set; }


		public Cliente()
		{
		}
	}
}

