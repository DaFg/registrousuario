﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Clientes.Models
{
	[Table("Telefono", Schema = "probando")]
	public class Telefono
	{
		[Column("Id")]
		public int ID { get; set; }

		[DisplayName("Numero Celular")]
		[Column("TelCelID")]
		public String NumeroCelular { get; set; }

		[DisplayName("Numero Hogar")]
		[Column("TelHogarID")]
		public String NumeroHogar { get; set; }

		[DisplayName("Numero Trabajo")]
		[Column("TelTrabajoID")]
		public String NumeroTrabajo { get; set; }

		[Column("Cliente_Id")]
		[ForeignKey("Cliente")]
		public int ClienteId { get; set; }


		public virtual Cliente Cliente { get; set; }


		public Telefono()
		{
		}
	}
}

